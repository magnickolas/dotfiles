vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.ignorecase = false
vim.opt.number = true
vim.opt.hidden = true
vim.opt.completeopt = "menuone,noinsert,noselect"
vim.opt.showmode = false
